package org.uk.linuxgrotto.service;

import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingController {
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@RequestMapping(value="/greeting", method=RequestMethod.GET, headers="Accept=application/xml, application/json")
	@ResponseBody
	public Response greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		
		return Response.ok().build();
	}
}
